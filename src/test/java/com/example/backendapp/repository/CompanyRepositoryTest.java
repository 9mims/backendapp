package com.example.backendapp.repository;

import com.example.backendapp.TestFixture;
import com.example.backendapp.data.Company;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CompanyRepositoryTest {


    @Autowired
    CompanyRepository companyRepository;

    TestFixture fixture = new TestFixture();

    public CompanyRepositoryTest() throws JsonProcessingException, JSONException {
    }

    @BeforeEach
    public void setup(){
        companyRepository.deleteAll();
    }
    @Test
    @Order(1)
    @Rollback(value = false)
    public void saveCompanyTest() throws JSONException {

    Company company = Company.builder()
            .id(1)
            .name("name")
            .contact(new JSONObject(fixture.contactJson))
            .addresses(new JSONArray(fixture.addressesJson))
            .build();

        companyRepository.save(company);

        Assertions.assertThat(company.getId()).isEqualTo(1);
    }
}