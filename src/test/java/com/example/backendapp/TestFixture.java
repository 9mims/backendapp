package com.example.backendapp;

import com.example.backendapp.data.Company;
import com.example.backendapp.data.CompanyDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TestFixture {

    private ObjectMapper mapper = new ObjectMapper();
    public final String addressesJson = "[{\"id\":0,\"street\":\"650 Funk Street\",\"streetName\":\"Johnston Springs\",\"buildingNumber\":\"719\",\"city\":\"Lake Delphiaton\",\"zipcode\":\"31989-0456\",\"country\":\"Gambia\",\"county_code\":\"AS\",\"latitude\":-56.526766,\"longitude\":-178.578624},{\"id\":0,\"street\":\"8890 Homenick Burg\",\"streetName\":\"Eldon Route\",\"buildingNumber\":\"354\",\"city\":\"Lindgrenbury\",\"zipcode\":\"77872-8812\",\"country\":\"Guyana\",\"county_code\":\"IQ\",\"latitude\":31.344051,\"longitude\":112.978033}]";
    public final String contactJson = "{\n" +
            "  \"id\": 0,\n" +
            "  \"firstname\": \"Geoffrey\",\n" +
            "  \"lastname\": \"Tremblay\",\n" +
            "  \"email\": \"mmclaughlin@kuhlman.com\",\n" +
            "  \"phone\": \"+7397253992302\",\n" +
            "  \"birthday\": \"1999-11-29\",\n" +
            "  \"gender\": \"male\",\n" +
            "  \"address\": {\n" +
            "    \"id\": 0,\n" +
            "    \"street\": \"153 Goldner Spurs Suite 994\",\n" +
            "    \"streetName\": \"Bosco Green\",\n" +
            "    \"buildingNumber\": \"518\",\n" +
            "    \"city\": \"Thompsonburgh\",\n" +
            "    \"zipcode\": \"78739\",\n" +
            "    \"country\": \"Madagascar\",\n" +
            "    \"county_code\": \"PM\",\n" +
            "    \"latitude\": 39.764585,\n" +
            "    \"longitude\": 8.754642\n" +
            "  },\n" +
            "  \"website\": \"http:\\/\\/wisozk.com\",\n" +
            "  \"image\": \"http:\\/\\/placeimg.com\\/640\\/480\\/people\"\n" +
            "}";

    public final CompanyDto companyDto1 = CompanyDto.builder()
            .id(1)
            .name("name1")
            .contact(mapper.readTree(contactJson))
            .addresses(mapper.readTree(addressesJson))
            .build();

    public final CompanyDto companyDto2 = CompanyDto.builder()
            .id(2)
            .name("name2")
            .contact(mapper.readTree(contactJson))
            .addresses(mapper.readTree(addressesJson))
            .build();

    public final CompanyDto companyDto3 = CompanyDto.builder()
            .id(3)
            .name("name3")
            .contact(mapper.readTree(contactJson))
            .addresses(mapper.readTree(addressesJson))
            .build();

    public final Company company1 = Company.builder()
            .id(1)
            .name("name1")
            .contact(new JSONObject(contactJson))
            .addresses(new JSONArray(addressesJson))
            .build();

    public final Company company2 = Company.builder()
            .id(2)
            .name("different_name2")
            .contact(new JSONObject(contactJson))
            .addresses(new JSONArray(addressesJson))
            .build();

    public final Company company5 = Company.builder()
            .id(5)
            .name("name5")
            .contact(new JSONObject(contactJson))
            .addresses(new JSONArray(addressesJson))
            .build();


    public TestFixture() throws JsonProcessingException, JSONException {
    }

    private Company toCompany(CompanyDto companyDto) throws JsonProcessingException, JSONException {
        Company company = new Company();
        company.setId(companyDto.getId());
        company.setName(companyDto.getName());
        company.setEmail(companyDto.getEmail());
        company.setVat(companyDto.getVat());
        company.setPhone(companyDto.getPhone());
        company.setCountry(companyDto.getCountry());
        company.setAddresses(new JSONArray(mapper.writeValueAsString(companyDto.getAddresses())));
        company.setImage(companyDto.getImage());
        company.setWebsite(companyDto.getWebsite());
        company.setContact(new JSONObject(mapper.writeValueAsString(companyDto.getContact())));
        return company;
    }

    public List<Company> toCompanies(List<CompanyDto> companyDtos) throws JsonProcessingException {
        return companyDtos.stream()
                .map(this::tryMapToCompany)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Company tryMapToCompany(CompanyDto dto) {
        try {
            return toCompany(dto);
        } catch (JsonProcessingException | JSONException e) {
            //log an error
        }
        return null;
    }

}
