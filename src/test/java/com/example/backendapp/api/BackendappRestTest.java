package com.example.backendapp.api;

import com.example.backendapp.BackendappApplicationTests;
import com.example.backendapp.data.CompanyDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.List;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class BackendappRestTest extends BackendappApplicationTests {

    @Autowired
    BackendappRestImpl backendappRest;

    @Test
    public void getCompaniesTest() throws URISyntaxException, JsonProcessingException {
        ResponseEntity<List<CompanyDto>> result = backendappRest.fetchDealerData();
        Assertions.assertEquals(200, result.getStatusCodeValue());
        Assertions.assertEquals(200, result.getBody().size());
    }
}