package com.example.backendapp.service;

import com.example.backendapp.TestFixture;
import com.example.backendapp.data.Company;
import com.example.backendapp.data.CompanyDto;
import com.example.backendapp.repository.CompanyRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest
public class BackendappServiceImplTest {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private BackendappServiceImpl service;

    TestFixture fixture = new TestFixture();

    public BackendappServiceImplTest() throws JsonProcessingException, JSONException {
    }

    @BeforeEach
    public void setup() throws JsonProcessingException {
        companyRepository.deleteAll();
    }

    @Test
    public void saveAll() throws JsonProcessingException {
        List<CompanyDto> companiesToBeSaved = new ArrayList<CompanyDto>();
        companiesToBeSaved.add(fixture.companyDto1);
        service.saveCompanies(companiesToBeSaved);

        List savedCompanies = (List) companyRepository.findAll();
        assertEquals(savedCompanies.size(), 1);
    }

    @Test
    public void synchronizeCompanyData() throws JsonProcessingException {
        List<Company> savedCompanies = Arrays.asList(fixture.company1, fixture.company2, fixture.company5);
        companyRepository.saveAll(savedCompanies);

        List<CompanyDto> fetchedDealerData = Arrays.asList(fixture.companyDto1, fixture.companyDto2, fixture.companyDto3);
        service.synchronizeCompanyData(fetchedDealerData);
        ArrayList<Company> newState = (ArrayList<Company>) companyRepository.findAll();
        assertEquals(fixture.toCompanies(fetchedDealerData), newState);
    }
}