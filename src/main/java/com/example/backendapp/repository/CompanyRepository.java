package com.example.backendapp.repository;

import com.example.backendapp.data.Company;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Integer> {

    @Query(value = "from Company c")
    Stream<Company> streamAll();
}
