package com.example.backendapp.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.json.JSONObject;

@NoArgsConstructor
@Getter
@Setter
public class AddressDto {
    private Integer id;
    private String street;
    private String streetName;
    private String buildingNumber;
    private String city;
    private String zipcode;
    private String country;
    @JsonProperty("county_code")
    private String countyCode;
    private Double latitude;
    private Double longitude;

    public AddressDto(JSONObject jsonObject) {
        AddressDto addressDto = new AddressDto();
        addressDto.setStreet(jsonObject.getString("street"));
        addressDto.setCity(jsonObject.getString("city"));
        addressDto.setZipcode(jsonObject.getString("zipcode"));
        addressDto.setCountry(jsonObject.getString("country"));
        addressDto.setLatitude(Double.valueOf(jsonObject.getString("latitude")));
        addressDto.setLatitude(Double.valueOf(jsonObject.getString("longitude")));
    }
}
