package com.example.backendapp.data;

import lombok.*;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "company")
public class Company implements Serializable {
    @Id
    private Integer id;
    private String name;
    private String email;
    private String vat;
    private String phone;
    private String country;
    @Convert(converter= JSONArrayConverter.class)
    private JSONArray addresses;
    private String website;
    private String image;
    @Convert(converter= JSONObjectConverter.class)
    private JSONObject contact;

    @Override
    public String toString() {
        return "[{\"id\":" + this.id + "," +
                "\"name\":" + this.name + "," +
                "\"email\":" + this.email +"," +
                "\"vat\":" + this.vat +"," +
                "\"phone\":"+ this.phone +"," +
                "\"country\":"+ this.country +"," +
                "\"addresses\":" +  new JSONArrayConverter().convertToDatabaseColumn(this.addresses) +
                "\"website\":" + this.website +"," +
                "\"image\":"+ this.image +"," +
                "\"contact\":" + new JSONObjectConverter().convertToDatabaseColumn(this.contact) +
                "}]";
    }
}
