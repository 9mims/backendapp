package com.example.backendapp.data;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Getter
@Setter
public class CompanyResponse implements Serializable {
    private String name;
    private String phone;
    private AddressDto address;
}
