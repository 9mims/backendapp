package com.example.backendapp.data;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ContactDto {
    private Integer id;
    private String firstname;
    private String lastname;
    private String email;
    private String phone;
    private Date birthday;
    private String gender;
    private AddressDto address;
    private String website;
    private String image;
}
