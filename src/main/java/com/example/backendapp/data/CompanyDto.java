package com.example.backendapp.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.*;

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Getter
@Setter
public class CompanyDto {
    private Integer id;
    private String name;
    private String email;
    private String vat;
    private String phone;
    private String country;
    //private List<AddressDto> addresses;
    @JsonProperty("addresses")
    private JsonNode addresses;
    private String website;
    private String image;
    //private ContactDto contact;
    @JsonProperty("contact")
    private JsonNode contact;

    @Override
    public String toString() {
        return "[{\"id\":" + this.id + "," +
                "\"name\":" + this.name + "," +
                "\"email\":" + this.email +"," +
                "\"vat\":" + this.vat +"," +
                "\"phone\":"+ this.phone +"," +
                "\"country\":"+ this.country +"," +
                "\"addresses\":" + this.addresses.toPrettyString() +
                "\"website\":" + this.website +"," +
                "\"image\":"+ this.image +"," +
                "\"contact\":" + this.contact.toPrettyString() +
                "}]";
    }
}
