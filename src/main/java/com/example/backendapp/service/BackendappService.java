package com.example.backendapp.service;

import com.example.backendapp.data.CompanyDto;
import com.example.backendapp.data.CompanyResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.transaction.Transactional;
import java.util.List;

public interface BackendappService {

    void saveCompanies(List<CompanyDto> companyDtos) throws JsonProcessingException;

    List<CompanyResponse> getCompanies() throws JsonProcessingException;

    @Transactional
    void synchronizeCompanyData(List<CompanyDto> fetchedData) throws JsonProcessingException;
}
