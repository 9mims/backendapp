package com.example.backendapp.service;


import com.example.backendapp.data.AddressDto;
import com.example.backendapp.data.Company;
import com.example.backendapp.data.CompanyDto;
import com.example.backendapp.data.CompanyResponse;
import com.example.backendapp.repository.CompanyRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BackendappServiceImpl implements BackendappService {

    @Autowired
    private CompanyRepository companyRepository;

    private ObjectMapper mapper = new ObjectMapper();


    @Override
    public void saveCompanies(List<CompanyDto> companyDtos) throws JsonProcessingException {
        companyRepository.saveAll(toCompanies(companyDtos));
    }

    @Transactional
    @Override
    public List<CompanyResponse> getCompanies() throws JsonProcessingException {
        return companyRepository.streamAll()
                .map(this::tryMapToCompanyResponse)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void synchronizeCompanyData(List<CompanyDto> fetchedData) throws JsonProcessingException {
        List<Company> dbData = (List<Company>) companyRepository.findAll();

        List<CompanyDto> changedAndAddedRows = getChangedAndAddedRows(fetchedData, dbData);
        saveCompanies(changedAndAddedRows);

        List<Integer> deletedRows = getDeletedRows(fetchedData, dbData);
        companyRepository.deleteAllById(deletedRows);
    }

    private List<Integer> getDeletedRows(List<CompanyDto> fetchedData, List<Company> dbData) {
        return dbData.stream().map(Company::getId).filter(a ->
                        fetchedData.stream().map(CompanyDto::getId)
                                .noneMatch(b -> isEqual(a, b))) // 1 2 3  1 2 5
                .distinct()
                .collect(Collectors.toList());
    }

    private List<CompanyDto> getChangedAndAddedRows(List<CompanyDto> fetchedData, List<Company> dbData) {
        List r = fetchedData.stream().filter(a ->
                        dbData.stream().noneMatch(b -> isEqual(a, b)))
                .distinct()
                .collect(Collectors.toList());
        return r;
    }

    private boolean isEqual(CompanyDto companyDto, Company company) {
        return Objects.equals(companyDto.toString(), company.toString());
    }

    private boolean isEqual(Integer companyDtoId, Integer companyId) {
        return companyDtoId.equals(companyId);
    }

    private CompanyResponse toCompanyResponse(Company company) throws JsonProcessingException {
        CompanyResponse companyResponse = new CompanyResponse();
        companyResponse.setName(company.getName());
        companyResponse.setPhone(company.getPhone());
        companyResponse.setAddress(readJsonArray(company.getAddresses()).get(0));
        return companyResponse;
    }

    private List<AddressDto> readJsonArray(JSONArray addresses) throws JsonProcessingException {
        List<AddressDto> list = new ArrayList<>();
        for (Object a : addresses) {
            JSONObject jsonObj = (JSONObject) a;
            AddressDto usr = mapper.readValue(jsonObj.toString(), AddressDto.class);
            list.add(usr);
        }
        return list;
    }

    private Company toCompany(CompanyDto companyDto) throws JsonProcessingException {
        Company company = new Company();
        company.setId(companyDto.getId());
        company.setName(companyDto.getName());
        company.setEmail(companyDto.getEmail());
        company.setVat(companyDto.getVat());
        company.setPhone(companyDto.getPhone());
        company.setCountry(companyDto.getCountry());
        company.setAddresses(new JSONArray(mapper.writeValueAsString(companyDto.getAddresses())));
        company.setImage(companyDto.getImage());
        company.setWebsite(companyDto.getWebsite());
        company.setContact(new JSONObject(mapper.writeValueAsString(companyDto.getContact())));
        return company;
    }

    public List<Company> toCompanies(List<CompanyDto> companyDtos) throws JsonProcessingException {
        return companyDtos.stream()
                .map(this::tryMapToCompany)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Company tryMapToCompany(CompanyDto dto) {
        try {
            return toCompany(dto);
        } catch (JsonProcessingException e) {
            //log an error
        }
        return null;
    }

    private CompanyResponse tryMapToCompanyResponse(Company company) {
        try {
            return toCompanyResponse(company);
        } catch (JsonProcessingException e) {
            //log an error
        }
        return null;
    }
}
