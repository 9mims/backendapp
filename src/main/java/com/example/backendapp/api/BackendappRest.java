package com.example.backendapp.api;

import com.example.backendapp.data.CompanyDto;
import com.example.backendapp.data.CompanyResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface BackendappRest {
    @RequestMapping(value = "/fetch", method = RequestMethod.GET)
    ResponseEntity<List<CompanyDto>> fetchDealerData() throws JsonProcessingException;

    @RequestMapping(value = "/synchronize", method = RequestMethod.GET)
    ResponseEntity<String> synchronizeCompanyData() throws JsonProcessingException;

    @RequestMapping(value = "/getCompanies", method = RequestMethod.GET)
    ResponseEntity<List<CompanyResponse>> getDealerData() throws JsonProcessingException;
}
