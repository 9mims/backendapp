package com.example.backendapp.api;

import com.example.backendapp.data.CompanyDto;
import com.example.backendapp.data.CompanyResponse;
import com.example.backendapp.service.BackendappService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.Refill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping
public class BackendappRestImpl implements BackendappRest{

    @Autowired
    private BackendappService service;

    @Value("${dealer.data.url}")
    private String dealerDataUrl;

    private final Bucket bucket;

    public BackendappRestImpl() {
        Bandwidth limit = Bandwidth.classic(20, Refill.greedy(20, Duration.ofMinutes(1)));
        this.bucket = Bucket4j.builder()
                .addLimit(limit)
                .build();
    }

    @Override
    @RequestMapping(value = "/fetch", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyDto>> fetchDealerData() throws JsonProcessingException {
        ResponseEntity<String> response = new RestTemplate().exchange(dealerDataUrl, HttpMethod.GET, null, String.class);
        return extractDealerData(response.getBody());
    }

    @Override
    @RequestMapping(value = "/synchronize", method = RequestMethod.GET)
    public ResponseEntity<String> synchronizeCompanyData() throws JsonProcessingException {
        if (bucket.tryConsume(1)) {
            ResponseEntity<List<CompanyDto>> fetchedData = fetchDealerData();
            service.synchronizeCompanyData(fetchedData.getBody());
        }
        return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
    }

    @Override
    @RequestMapping(value = "/getCompanies", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyResponse>> getDealerData() throws JsonProcessingException {
        if (bucket.tryConsume(1)) {
            return new ResponseEntity<>(service.getCompanies(), HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
    }

    private ResponseEntity<List<CompanyDto>> extractDealerData(String resonseBody) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        List<CompanyDto> companies = new ArrayList<>();
        try {
            String json = new JSONObject(resonseBody).getJSONArray("data").toString();
            companies = mapper.readValue(json, new TypeReference<List<CompanyDto>>() {});
        } catch (Exception e) {
            //log an error message
        }
        return new ResponseEntity<>(companies, HttpStatus.OK);
    }
}


